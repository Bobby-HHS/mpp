import { createRouter, createWebHistory } from '@ionic/vue-router';
// import { RouteRecordRaw } from 'vue-router';


const routes= [
  {
    path: '',
    redirect: '/home',
  },
  // {
  //   path: '/auth',
  //   name: 'Login',
  //   component: () => import ('../views/LoginOverview.vue')
  // },  
  {
    path: '/:patchMatch(.*)*',
    name: 'not-found',
    component: () => import ('../views/NotFound.vue'),
  },

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
