import { createApp } from 'vue'
import { createStore } from 'vuex'
import App from './App.vue'
import router from './router';

import { IonicVue } from '@ionic/vue';

/* Core CSS required for Ionic components to work properly */
import '@ionic/vue/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/vue/css/normalize.css';
import '@ionic/vue/css/structure.css';
import '@ionic/vue/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/vue/css/padding.css';
import '@ionic/vue/css/float-elements.css';
import '@ionic/vue/css/text-alignment.css';
import '@ionic/vue/css/text-transformation.css';
import '@ionic/vue/css/flex-utils.css';
import '@ionic/vue/css/display.css';

/* Theme variables */
import './theme/variables.css';

// import Vue from 'vue';


const store = createStore({
  state () {
    return {
      apiURL:"https://groep7.azurewebsites.net/api/v1",
      JWT: null,
      sessionLoggedIn: false,
      selectedModule: null,
      userId: null,
      isTeacher: false,
      id: null,
      shouldUpdate: false,
    }
  },
  mutations: {

    setShouldUpdate(state, newValue){
      state.shouldUpdate = newValue;
    },

    toggleModule (state, moduleId) {
      state.selectedModule = moduleId;
    },
    toggleAssignment (state, assignmentId) {
      state.selectedAssignment = assignmentId;
    },
    setUser (state, userId) {
      state.userId = userId;
    },
    setTeacher (state, isTeacher) {
      state.isTeacher = isTeacher;
    },
    logout(state) {

      console.log('logout' );
      state.sessionLoggedIn = false,
      state.selectedModule = null,
      state.userId = null,
      state.isTeacher = false,
      state.id = null
    },

  },
})

const app = createApp(App)
  .use(IonicVue)
  .use(router)
  .use(store)
  
router.isReady().then(() => {
  app.mount('#app');
});
