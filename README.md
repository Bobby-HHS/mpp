# front-end

Webapplications Front-end, <br/>
Beoordelings applicatie


## Auteurs
Ronald Brand <br/>
Matthew Akinbile <br/>
Jorne te Nijenhuis <br/>
Bobby de Jong <br/>

## Link naar deployment pages
- https://thuas.gitlab.io/dt-webapplications/wa-2122-s4/intervisiegroepen/groep-7/webapplication/front-end/

## Versies
- 0.1 basis skelet van de applicatie. <br />
- 0.2 functionele front-end applicatie. <br />

## Huidige versie:
- 0.5 connectie maken met de backend. <br />

## toekomstige Versies
- 0.6 kunnen inloggen als student en als docent. <br />
- 0.7 data kunnen verzenden en ontvangen van de backend op de correcte manier. <br />
- 0.8 dynamisch correcte data laten zien op de correcte pagina's. <br />
- 0.9 aanpassingen maken op de front-end met correcte communicatie met de backend. <br />

## styling, to do
- veranderd website naar huisstyle. <br />
- applicatie blijft bruikbaar na verandering grote van browser. <br />


## To do
- puntjes op de i zetten kwa styling.
- opschonen code.

## Status
90% gereed. <br />

## Bronnen
- filmpjes op blackboard van Traversy Media. <br />
- youtube kanaal van: The Net Ninja.<br />
- groepsgenoten voor feedback.<br />
- opname van de college's.<br />
- Ionic documentatie. <br />

## Gebruikte Library's
- VUEX: vuex is gebruikt voor state management van de tabel, <br />
    - reden: iedereen kon het meteen begrijpen, waardoor het makkelijk was om te gebruiken als groep. <br />
- Ionic: Een open source toolkit om cross-platform 'web' applicaties te bouwen. <br />
    - reden: We hebben gekozen om Ionic te gebruiken omdat het veel styling uit handen neemt. Ook zorgt het gebruik van Ionic ervoor dat de applicatie een uniform 'look' heeft. 



