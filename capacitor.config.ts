import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'front-end',
  webDir: 'dist',
  bundledWebRuntime: false
};

export default config;
